(** 
 * File for image pre-treatment including edge detection, hough transform and
 * rotation 
 *
 * Dependencies : Sdl, Sdlevent, Sdlvideo, Bigarray
 **)

let dims_img img =
    ((Sdlvideo.surface_info img).Sdlvideo.w, (Sdlvideo.surface_info
    img).Sdlvideo.h)

let max x y =
    if x > y then x else y

let pi = 2. *. acos(-1.)

let rad_to_deg = function angle -> (angle *. 180.) /. pi

let deg_to_rad = function angle -> (angle *. pi) /. 180.

let rot_n (a0, b0) (a, b) angle n =
    let cos_ang = cos angle in
    let sin_ang = sin angle in
        int_of_float (cos_ang*.(float (a-a0))+.
        n*.sin_ang*.(float (b-b0))+.(float a0))

let rot_x (x0, y0) (x, y) angle =
    rot_n (x0, y0) (x, y) angle (-1.)

let rot_y (x0, y0) (x, y) angle =
    rot_n (y0, x0) (y, x) angle (1.)

let positive_angle angle =
  if angle < 0. then
    angle +. 360.
  else angle

let rotation src dst angle =
  let (w_src, h_src) = dims_img src in
  let ang = pi *. (angle /. 180.) in
  let (x_center, y_center) = ((w_src / 2), (h_src / 2)) in
    for j = 0 to (h_src - 1) do
        for i = 0 to (w_src - 1) do
            let rotx = rot_x (x_center, y_center) (i, j) (-.ang) in
            let roty = rot_y (x_center, y_center) (i, j) (-.ang) in
            if (i >= 0 && i <= (w_src - 1) && j >= 0 && j <= (h_src -1)) then
                if (rotx >= 0 && rotx <= (w_src - 1) && roty >= 0 && roty <=
                    (h_src - 1)) then
                        Sdlvideo.put_pixel_color dst i j
                        (Sdlvideo.get_pixel_color src rotx roty)
        done
    done



let init_image (width, height) = 
    let img = Sdlvideo.create_RGB_surface [] width height 8 Int32.max_int
    Int32.max_int Int32.max_int Int32.zero in 
    for i = 0 to width-1 do
      for j = 0 to height-1 do
        Sdlvideo.put_pixel_color img i j (255,255,255);
      done;
    done;
    img

let max_matrix matrix w h =
    let maxpos = ref(0,0) in
    let max = ref 0 in
        for j=0 to (h-1) do
            for i =0 to (w-1) do
                if (!max < matrix.(i).(j)) then
                    begin
                        maxpos := (i,j);
                        max := matrix.(i).(j)
                    end
            done;
        done;
    !maxpos

let hough_transform img =
    let w = ((Sdlvideo.surface_info img).Sdlvideo.w) in
    let h = ((Sdlvideo.surface_info img).Sdlvideo.h) in
    let rad = pi /. 180. in
    let pmax = int_of_float (sqrt(float(w*w) +. float(h*h)) +. 1.) in
    let co theta = cos(float(theta) *.rad) in
    let si theta = sin(float(theta) *.rad) in
    let hough_transform_matrix = Array.make_matrix pmax 181 0 in
        for i=0 to w-1 do
            for j=0 to h-1 do
                if (Sdlvideo.get_pixel_color img i j)=(0,0,0) then
                    for theta = 0 to 180 do
                        let p = int_of_float(float(i)*.co (theta)
                        +.float(j)*.si (theta)) in
                        if (p >= 0) then
                            hough_transform_matrix.(p).(theta) <-
                                hough_transform_matrix.(p).(theta)+1
                        else
                            hough_transform_matrix.(-p).(theta) <-
                                hough_transform_matrix.(-p).(theta)+1
                    done;
            done;
        done;
    let (a, b) = max_matrix hough_transform_matrix pmax 181 in
    (90 - b)

(******************************************************************************)
(* The following functions are here for testing purpose *)

let sdl_init () =
    begin
        Sdl.init [`EVERYTHING];
        Sdlevent.enable_events Sdlevent.all_events_mask;
    end

let rec wait_key () =
    let e = Sdlevent.wait_event () in
        match e with
            Sdlevent.KEYDOWN _ -> ()
            | _ -> wait_key ()
        
let show img dst =
    let d = Sdlvideo.display_format img in
        Sdlvideo.blit_surface d dst ();
        Sdlvideo.flip dst

let main () =
    begin
        if Array.length (Sys.argv) < 2 then
            failwith "Je veux mon fichier merde.";
            sdl_init ();
            let img = Sdlloader.load_image Sys.argv.(1) in
            let (w0,h0) = dims_img img in
            let display = Sdlvideo.set_video_mode h0 w0 [`DOUBLEBUF] in
            let very_new_img = init_image (w0, h0) in
            show img display;
            wait_key ();
            (*let angle = hough_transform img in*)
            let angle =  (-.0.8) in
            rotation img very_new_img angle;
            show very_new_img display;
            wait_key ();
            Sdlvideo.save_BMP very_new_img "lolwat.bmp";
            exit 0
    end

let main2 binarizedImg ang =
    begin
            sdl_init ();
            let img = binarizedImg in
            let (w0,h0) = dims_img img in
            let display = Sdlvideo.set_video_mode h0 w0 [`DOUBLEBUF] in
            let very_new_img = init_image (w0, h0) in
            show img display;
            wait_key ();
            (*let angle = hough_transform img in*)
            let angle =  (ang) in
            rotation img very_new_img (float angle);
            show very_new_img display;
            wait_key ();
            Sdlvideo.save_BMP very_new_img "lolwat.bmp";
            very_new_img
    end
let _ = main ()
