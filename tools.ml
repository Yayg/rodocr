(*****************************************************************************)
(*
*	This is the toolbox module
*	No dependencies yet
 *****************************************************************************)

let mult a b = a * b (* Thanks Captain Obvious *)

let rec sumList = function (* For a list like [a;b;c] returns a+b+c *)
	| [] -> 0
	| h :: t -> h + sumList t

let neuronEval arrayE arrayW =
    let e = ref 0. in
    for i=0 to (Array.length arrayE - 1) do
        e := !e +. arrayE.(i) *. arrayW.(i)
    done;
    !e

let sigmo x = if x > 0.5 then 1. else 0.

let sigmoDerivate x = 
    (Pervasives.exp (0.5-.x))/.((1. +. Pervasives.exp (0.5-.x))**2.)
