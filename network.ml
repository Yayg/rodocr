(******************************************************************************)
(*
*	This is the network module
*	Dependencies: Tool, List, Random.
*)

(* A single neuron *)
 
Random.init 42;

class perceptron wList f thres =
	object(self)
		val mutable wL = wList

		method getWeigths = wL
		method backprop i newW =
			let rec browser n j = function
				| [] -> invalid_arg "List of weight too short"
				| a :: b when j = 1 -> n :: b
				| a :: b -> a :: browser n (i-1) b
			in wL <- browser newW i wL
		method sum inputList =
			Tools.sumList (List.map2 Tools.mult inputList wL)
		method getOutput inputList =
        		let s = self#sum inputList in
           		if ((f s) > thres)
			then 1
        	   	else 0
		method learn testList (* (int list * int) list *) =
			let numTest = List.length testList in
			if (numTest <> (List.length wL)) then

			invalid_arg "Bad learning list."

			else
			
			let passed = ref 0 in
			while (!passed <> numTest) do
			
			passed := 0;
			let l = ref testList in

			while (!l <> []) do
			
			match !l with  ((entries, out) :: t) ->
			let output = self#getOutput entries in
			
			begin
			if (output <> out) then
			let diff = out - output in
			let rec browser entr weights = match (entr,weights) with
				| ([],[]) -> []
				| (a :: b, c :: d) ->
					(c + diff * a) :: (browser b d)
                                | (_,_) -> failwith "42"
			in wL <- browser entries wL
			
			else
			passed := !passed + 1
			end;
			l := t

			| _ -> failwith "Bad testList"

			done;
			done;

	end



(* MLP... GTFO! there is no pony here ! *)
 
class neuron nbEntries =
        object(self)
            val mutable precEntries = Array.make nbEntries 0.
            val mutable weights = Array.make nbEntries 0.
            val mutable error = 1.
            
            initializer
            self#initW;

            method initW =
                let m = weights in
                for i=0 to (nbEntries-1) do
                    m.(i) <- Random.float (2./.81.)
                done;
                weights <- m (* Parano *)

            method isNeuron = true
            method getError = error
            method setError x = error <- x
            method getWeigths = weights
            method updateWeight w = weights <- w
            method getLastEntries = precEntries
	    method sum input =
	    	Tools.neuronEval input weights
	    method output input =
                if (Array.length input <> nbEntries ) then
                    failwith "Bad neuron input."
                else
                    precEntries <- input;
                    let s = self#sum input in
           	    Tools.sigmo s
	end

class network =
	object(self)
            val mutable layers = Array.make 3 (None)
            
            initializer
            self#makeLayers
            
            method getLayers = layers

            method makeLayers =
                let l1 = self#makeLayer 38 81 in
                let l2 = self#makeLayer 18 38  in
                let l3 = self#makeLayer 8 18 in
                layers.(0) <- Some l1;
                layers.(1) <- Some l2;
                layers.(2) <- Some l3
                 
            method makeLayer nbNeuron prec =
                let l1 = Array.make nbNeuron (None) in
                for i=0 to (nbNeuron-1) do
                    l1.(i) <- Some (new neuron prec)
                done;
                l1

            method getLayerOutput input l =
         
                let out = Array.make (Array.length l) 0. in
                for i=0 to (Array.length l - 1) do
                    out.(i) <- 
                        (match l.(i) with Some(n:neuron) -> n#output input
                            | _ -> failwith "Neuron not initialized")
                done;
                out

            method getNetworkOutput input =
                let res = ref (Some input) in
                for i=0 to 2 do
                    match (layers.(i)) with 
                    | Some(l) -> res := Some (self#getLayerOutput (match !res with
                    Some(x) -> x | None -> failwith "42! (Garde fou 1)") l)
                    | _ -> failwith "Network not initialized." 
                done;
                match !res with Some(x) -> x | _ -> failwith "WTF (Garde fou 2)"

            method setOutputError lastLayer expectedOut =
                for i=0 to (Array.length lastLayer - 1) do
                        match lastLayer.(i) with
                            | Some(n:neuron) -> let x = Tools.sigmoDerivate (n#sum
                            (n#getLastEntries)) *. 
                            ((n#output n#getLastEntries) -. expectedOut.(i)) in
                            n#setError x;
                            | _ -> failwith "43! (Garde fou 3)"
                done

            method backpropagate l2 l1 =
                for j=0 to (Array.length l1 - 1) do
                    match l1.(j) with 
                    | Some(n:neuron) -> 
                    let out = (n#sum n#getLastEntries) in
                    let a = Tools.sigmoDerivate out in
                    let s = ref 0. in
                    for i=0 to (
                        Array.length l2 - 1) do
                        match l2.(i) with 
                            | Some(m:neuron) -> 
                                s := !s +. m#getWeigths.(j) *. m#getError;
                                let w = m#getWeigths in
                                (*self#displayOut w;*)
                                w.(j) <- w.(j) -. (0.5) *. m#getError *. (out);
                                (*self#displayOut w;
                                print_string ((string_of_float w.(j))^"\n");
                                flush_all ();
                                m#updateWeight w*)
                            | _ -> failwith "Wrong layer"
                    done;
                    n#setError (a *. !s)
                    
                    | _ -> failwith "Corwin lache ce clavier. (Garde fou 4)"
                done

            method updateFirstLayer input =
                let l0 = match layers.(0) with Some(x) -> x | _ -> failwith "GF 42"
                in
                for i=0 to (Array.length l0 - 1) do
                    match l0.(i) with
                    | Some(n) -> 
                            let w = n#getWeigths in
                            for j=0 to (Array.length w - 1) do
                                w.(j) <- w.(j) -. (0.5) *. n#getError *.
                                input.(j);
                            done;
                            n#updateWeight w;
                    | _ -> failwith "Jaime pas les Warnings (GF 43)"
                done

            method learn c thOutput =
                (* let k = ref (-1) in *)
                let notLearn = ref true in
                let d = ref 0 in 
                while (!notLearn && (!d < 10)) do
                    d := !d + 1;
                    (* Display zone 
                    if !k = 9 then k:= 0 else k:=!k+1;*)
                    let out = self#getNetworkOutput c in 
                    (*let w = (match layers.(0) with
                                | Some(x) -> (match x.(6) with
                                                | Some(y:neuron) -> y#getWeigths
                                                | _ -> failwith "hum hum (GF
                                                8)")
                                | _ -> failwith "hum (GF 9)")
                    in 
                    let e = (match layers.(1) with
                                | Some(x) -> (match x.(!k) with
                                                | Some(y:neuron) -> y#getError
                                                | _ -> failwith "hum hum (GF
                                                8)")
                                | _ -> failwith "hum (GF 9)")
                    in 
                    
                    print_string (string_of_float (e)^" - ");
                    self#displayOut w;
                    print_endline ((self#toChar thOutput)^": \n");
                    self#displayOut out;*)

                    (* Learn zone *) 
                    if out = thOutput then
                        notLearn := false
                    else
                        self#setOutputError 
                            (match layers.(2) with 
                                | Some(x) -> x 
                                | _ -> failwith "Ceci est une erreur (Garde fou
                                5)")
                        thOutput;
                        for i=(Array.length layers - 1) downto 1 do
                            self#backpropagate
                            (match layers.(i) with 
                                | Some(x) -> x 
                                | _ -> failwith "Ceci est une erreur (Garde fou
                                6)")     
                            (match layers.(i-1) with 
                                | Some(y) -> y 
                                | _ -> failwith "Ceci est une erreur (Garde fou
                                7)")
                        done;
                        self#updateFirstLayer c
                done
            
             method toChar a = 
                match a with
                | [|0.;0.;0.;0.;0.;0.;0.;0.|] -> " "
                | [|0.;0.;0.;0.;0.;0.;0.;1.|] -> "a"
                | [|0.;0.;0.;0.;0.;0.;1.;0.|] -> "b"
                | [|0.;0.;0.;0.;0.;0.;1.;1.|] -> "c"
                | [|0.;0.;0.;0.;0.;1.;0.;0.|] -> "d"
                | [|0.;0.;0.;0.;0.;1.;0.;1.|] -> "e"
                | [|0.;0.;0.;0.;0.;1.;1.;0.|] -> "f"
                | [|0.;0.;0.;0.;0.;1.;1.;1.|] -> "g"
                | [|0.;0.;0.;0.;1.;0.;0.;0.|] -> "h"
                | [|0.;0.;0.;0.;1.;0.;0.;1.|] -> "i"
                | [|0.;0.;0.;0.;1.;0.;1.;0.|] -> "j"
                | [|0.;0.;0.;0.;1.;0.;1.;1.|] -> "k"
                | [|0.;0.;0.;0.;1.;1.;0.;0.|] -> "l"
                | [|0.;0.;0.;0.;1.;1.;0.;1.|] -> "m"
                | [|0.;0.;0.;0.;1.;1.;1.;0.|] -> "n"
                | [|0.;0.;0.;0.;1.;1.;1.;1.|] -> "o"
                | [|0.;0.;0.;1.;0.;0.;0.;0.|] -> "p"
                | [|0.;0.;0.;1.;0.;0.;0.;1.|] -> "q"
                | [|0.;0.;0.;1.;0.;0.;1.;0.|] -> "r"
                | [|0.;0.;0.;1.;0.;0.;1.;1.|] -> "s"
                | [|0.;0.;0.;1.;0.;1.;0.;0.|] -> "t"
                | [|0.;0.;0.;1.;0.;1.;0.;1.|] -> "u"
                | [|0.;0.;0.;1.;0.;1.;1.;0.|] -> "v"
                | [|0.;0.;0.;1.;0.;1.;1.;1.|] -> "w"
                | [|0.;0.;0.;1.;1.;0.;0.;0.|] -> "x"
                | [|0.;0.;0.;1.;1.;0.;0.;1.|] -> "y"
                | [|0.;0.;0.;1.;1.;0.;1.;0.|] -> "z"
                | [|0.;0.;0.;1.;1.;0.;1.;1.|] -> "A"
                | [|0.;0.;0.;1.;1.;1.;0.;0.|] -> "B"
                | [|0.;0.;0.;1.;1.;1.;0.;1.|] -> "C"
                | [|0.;0.;0.;1.;1.;1.;1.;0.|] -> "D"
                | [|0.;0.;0.;1.;1.;1.;1.;1.|] -> "E"
                | [|0.;0.;1.;0.;0.;0.;0.;0.|] -> "F"
                | [|0.;0.;1.;0.;0.;0.;0.;1.|] -> "G"
                | [|0.;0.;1.;0.;0.;0.;1.;0.|] -> "H"
                | [|0.;0.;1.;0.;0.;0.;1.;1.|] -> "I"
                | [|0.;0.;1.;0.;0.;1.;0.;0.|] -> "J"
                | [|0.;0.;1.;0.;0.;1.;0.;1.|] -> "K"
                | [|0.;0.;1.;0.;0.;1.;1.;0.|] -> "L"
                | [|0.;0.;1.;0.;0.;1.;1.;1.|] -> "M"
                | [|0.;0.;1.;0.;1.;0.;0.;0.|] -> "N"
                | [|0.;0.;1.;0.;1.;0.;0.;1.|] -> "O"
                | [|0.;0.;1.;0.;1.;0.;1.;0.|] -> "P"
                | [|0.;0.;1.;0.;1.;0.;1.;1.|] -> "Q"
                | [|0.;0.;1.;0.;1.;1.;0.;0.|] -> "R"
                | [|0.;0.;1.;0.;1.;1.;0.;1.|] -> "S"
                | [|0.;0.;1.;0.;1.;1.;1.;0.|] -> "T"
                | [|0.;0.;1.;0.;1.;1.;1.;1.|] -> "U"
                | [|0.;0.;1.;1.;0.;0.;0.;0.|] -> "V"
                | [|0.;0.;1.;1.;0.;0.;0.;1.|] -> "W"
                | [|0.;0.;1.;1.;0.;0.;1.;0.|] -> "X"
                | [|0.;0.;1.;1.;0.;0.;1.;1.|] -> "Y"
                | [|0.;0.;1.;1.;0.;1.;0.;0.|] -> "Z"
                | [|0.;0.;1.;1.;0.;1.;0.;1.|] -> "0"
                | [|0.;0.;1.;1.;0.;1.;1.;0.|] -> "1"
                | [|0.;0.;1.;1.;0.;1.;1.;1.|] -> "2"
                | [|0.;0.;1.;1.;1.;0.;0.;0.|] -> "3"
                | [|0.;0.;1.;1.;1.;0.;0.;1.|] -> "4"
                | [|0.;0.;1.;1.;1.;0.;1.;0.|] -> "5"
                | [|0.;0.;1.;1.;1.;0.;1.;1.|] -> "6"
                | [|0.;0.;1.;1.;1.;1.;0.;0.|] -> "7"
                | [|0.;0.;1.;1.;1.;1.;0.;1.|] -> "8"
                | [|0.;0.;1.;1.;1.;1.;1.;0.|] -> "9"
                | [|0.;0.;1.;1.;1.;1.;1.;1.|] -> "."
                | [|0.;1.;0.;0.;0.;0.;0.;0.|] -> "!"
                | _ -> " "

            method toBin a = 
                match a with
                | " " -> [|0.;0.;0.;0.;0.;0.;0.;0.|] 
                | "a" -> [|0.;0.;0.;0.;0.;0.;0.;1.|] 
                | "b" -> [|0.;0.;0.;0.;0.;0.;1.;0.|]
                | "c" -> [|0.;0.;0.;0.;0.;0.;1.;1.|] 
                | "d" -> [|0.;0.;0.;0.;0.;1.;0.;0.|] 
                | "e" -> [|0.;0.;0.;0.;0.;1.;0.;1.|] 
                | "f" -> [|0.;0.;0.;0.;0.;1.;1.;0.|] 
                | "g" -> [|0.;0.;0.;0.;0.;1.;1.;1.|] 
                | "h" -> [|0.;0.;0.;0.;1.;0.;0.;0.|] 
                | "i" -> [|0.;0.;0.;0.;1.;0.;0.;1.|] 
                | "j" -> [|0.;0.;0.;0.;1.;0.;1.;0.|] 
                | "k" -> [|0.;0.;0.;0.;1.;0.;1.;1.|] 
                | "l" -> [|0.;0.;0.;0.;1.;1.;0.;0.|] 
                | "m" -> [|0.;0.;0.;0.;1.;1.;0.;1.|] 
                | "n" -> [|0.;0.;0.;0.;1.;1.;1.;0.|] 
                | "o" -> [|0.;0.;0.;0.;1.;1.;1.;1.|] 
                | "p" -> [|0.;0.;0.;1.;0.;0.;0.;0.|] 
                | "q" -> [|0.;0.;0.;1.;0.;0.;0.;1.|] 
                | "r" -> [|0.;0.;0.;1.;0.;0.;1.;0.|] 
                | "s" -> [|0.;0.;0.;1.;0.;0.;1.;1.|] 
                | "t" -> [|0.;0.;0.;1.;0.;1.;0.;0.|] 
                | "u" -> [|0.;0.;0.;1.;0.;1.;0.;1.|] 
                | "v" -> [|0.;0.;0.;1.;0.;1.;1.;0.|] 
                | "w" -> [|0.;0.;0.;1.;0.;1.;1.;1.|] 
                | "x" -> [|0.;0.;0.;1.;1.;0.;0.;0.|] 
                | "y" -> [|0.;0.;0.;1.;1.;0.;0.;1.|] 
                | "z" -> [|0.;0.;0.;1.;1.;0.;1.;0.|] 
                | "A" -> [|0.;0.;0.;1.;1.;0.;1.;1.|] 
                | "B" -> [|0.;0.;0.;1.;1.;1.;0.;0.|] 
                | "C" -> [|0.;0.;0.;1.;1.;1.;0.;1.|] 
                | "D" -> [|0.;0.;0.;1.;1.;1.;1.;0.|]
                | "E" -> [|0.;0.;0.;1.;1.;1.;1.;1.|] 
                | "F" -> [|0.;0.;1.;0.;0.;0.;0.;0.|] 
                | "G" -> [|0.;0.;1.;0.;0.;0.;0.;1.|] 
                | "H" -> [|0.;0.;1.;0.;0.;0.;1.;0.|] 
                | "I" -> [|0.;0.;1.;0.;0.;0.;1.;1.|] 
                | "J" -> [|0.;0.;1.;0.;0.;1.;0.;0.|] 
                | "K" -> [|0.;0.;1.;0.;0.;1.;0.;1.|]
                | "L" -> [|0.;0.;1.;0.;0.;1.;1.;0.|] 
                | "M" -> [|0.;0.;1.;0.;0.;1.;1.;1.|] 
                | "N" -> [|0.;0.;1.;0.;1.;0.;0.;0.|] 
                | "O" -> [|0.;0.;1.;0.;1.;0.;0.;1.|] 
                | "P" -> [|0.;0.;1.;0.;1.;0.;1.;0.|] 
                | "Q" -> [|0.;0.;1.;0.;1.;0.;1.;1.|] 
                | "R" -> [|0.;0.;1.;0.;1.;1.;0.;0.|] 
                | "S" -> [|0.;0.;1.;0.;1.;1.;0.;1.|] 
                | "T" -> [|0.;0.;1.;0.;1.;1.;1.;0.|] 
                | "U" -> [|0.;0.;1.;0.;1.;1.;1.;1.|] 
                | "V" -> [|0.;0.;1.;1.;0.;0.;0.;0.|] 
                | "W" -> [|0.;0.;1.;1.;0.;0.;0.;1.|] 
                | "X" -> [|0.;0.;1.;1.;0.;0.;1.;0.|] 
                | "Y" -> [|0.;0.;1.;1.;0.;0.;1.;1.|] 
                | "Z" -> [|0.;0.;1.;1.;0.;1.;0.;0.|] 
                | "0" -> [|0.;0.;1.;1.;0.;1.;0.;1.|] 
                | "1" -> [|0.;0.;1.;1.;0.;1.;1.;0.|] 
                | "2" -> [|0.;0.;1.;1.;0.;1.;1.;1.|] 
                | "3" -> [|0.;0.;1.;1.;1.;0.;0.;0.|] 
                | "4" -> [|0.;0.;1.;1.;1.;0.;0.;1.|] 
                | "5" -> [|0.;0.;1.;1.;1.;0.;1.;0.|] 
                | "6" -> [|0.;0.;1.;1.;1.;0.;1.;1.|] 
                | "7" -> [|0.;0.;1.;1.;1.;1.;0.;0.|] 
                | "8" -> [|0.;0.;1.;1.;1.;1.;0.;1.|] 
                | "9" -> [|0.;0.;1.;1.;1.;1.;1.;0.|] 
                | "." -> [|0.;0.;1.;1.;1.;1.;1.;1.|]
                | "!" -> [|0.;1.;0.;0.;0.;0.;0.;0.|]
                | _ ->  [|0.;0.;0.;0.;0.;0.;0.;0.|]


            method displayOut o =
                print_string "[| ";
                for i=0 to (Array.length o - 2) do
                    print_string (" "^(string_of_float o.(i))^",")
                done;
                print_string (" "^(string_of_float (o.(Array.length o -
                1)))^"|] \n\n")

            (* Learning zone *)
            method learning alphabetM alphabetC =
                print_endline "Begining learning by neural network...";
                let notLearn = ref true in
                let d = ref 0 in 
                while (!notLearn && (!d < 20)) do
                    d := !d +1;
                    print_endline "New learning...";
                    let isLearn = ref true in
                    for i=0 to (Array.length alphabetM - 1) do
                        self#learn (alphabetM.(i)) (self#toBin alphabetC.(i))
                    done;
                    for i=0 to (Array.length alphabetM - 1) do
                        let out = self#getNetworkOutput (alphabetM.(i)) in
                        let charOut = self#toChar out in
                        isLearn := !isLearn  && (charOut = alphabetC.(i));
                        print_endline (alphabetC.(i)^" -> "^charOut);
                    done;
                    notLearn := not !isLearn
                done
        end 


(** Functions *****************************************************************)
